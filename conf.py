# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# http://www.sphinx-doc.org/en/master/config
# -- Path setup --------------------------------------------------------------
# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))
import platform
from datetime import datetime
from zoneinfo import ZoneInfo

import sphinx
import sphinx_material

project = "CNT-AIT"
html_title = project

author = f"Scribe"
html_logo = "images/logo_fa.jpg"
html_favicon = "images/logo_fa.jpg"
release = "0.1.0"
now = datetime.now(tz=ZoneInfo("Europe/Paris"))
version = f"{now.year}-{now.month:02}-{now.day:02} {now.hour:02}H ({now.tzinfo})"
today = version

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.doctest",
    "sphinx.ext.extlinks",
    "sphinx.ext.intersphinx",
    "sphinx.ext.todo",
    "sphinx.ext.mathjax",
    "sphinx.ext.viewcode",
    "sphinx_copybutton",
]
autosummary_generate = True
autoclass_content = "class"

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]
html_static_path = ["_static"]
html_show_sourcelink = True
html_sidebars = {
    "**": ["logo-text.html", "globaltoc.html", "localtoc.html", "searchbox.html"]
}
extensions.append("sphinx_material")
html_theme_path = sphinx_material.html_theme_path()
html_context = sphinx_material.get_html_context()
html_theme = "sphinx_material"

extensions.append("sphinx.ext.intersphinx")
intersphinx_mapping = {
    "antisem": ("https://luttes.frama.io/contre/l-antisemitisme/", None),
}
extensions.append("sphinx.ext.todo")
todo_include_todos = True

# material theme options (see theme.conf for more information)
html_theme_options = {
    "base_url": "https://anarchisme.frama.io/cnt-ait/",
    "repo_url": "https://framagit.org/anarchisme/cnt-ait",
    "repo_name": project,
    "html_minify": False,
    "html_prettify": True,
    "css_minify": True,
    "repo_type": "gitlab",
    "globaltoc_depth": -1,
    "color_primary": "black",
    "color_accent": "cyan",
    "touch_icon": "images/cnt.jpg",
    "theme_color": "#2196f3",
    "master_doc": False,
    "nav_title": f"{project} ({today})",
    "nav_links": [
        {
            "href": "genindex",
            "internal": True,
            "title": "Index",
        },
        {
            "href": "https://monde-libertaire.fr/",
            "internal": False,
            "title": "Monde Libertaire",
        },
        {
            "href": "https://www.radio-libertaire.org/agenda/",
            "internal": False,
            "title": "Agenda Radio Libertaire",
        },
        {
            "href": "https://anarchiste.info/stream/radiolib",
            "internal": False,
            "title": "Radio Libertaire",
        },
        {
            "href": "https://cntf.frama.io/linkertree/",
            "internal": False,
            "title": "Liens CNT-F",
        },
    ],
    "heroes": {
        "index": "CNT-AIT",
    },
    "table_classes": ["plain"],
}

language = "fr"
html_last_updated_fmt = ""

todo_include_todos = True

html_use_index = True
html_domain_indices = True
html_css_files = [
    "css/custom.css",
]

copyright = f"2023-{now.year}, {author} Built with sphinx {sphinx.__version__} Python {platform.python_version()}"
